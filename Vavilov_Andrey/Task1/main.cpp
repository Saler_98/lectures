#include <iostream>
using namespace std;

class User1 {
public:
    virtual void turn_on_phone1() = 0;
};

class Phone1: public User1 {
public:
    void turn_on_phone1() override {
        cout << "User1 Device is on" << endl;
    }
    void call()  {
        cout << "User1 is calling" << endl;
    }
};

class User2 {
public:
    virtual void turn_on_phone2() = 0;
};

class Phone2: public User2 {
public:
    void turn_on_phone2() override {
        cout << "User2 Device is on" << endl;
    }
    void browsing_internet()  {
        cout << "User 2 browses the Internet" << endl;
    }
};

class User3 {
public:
    virtual void turn_on_phone3() = 0;
};

class Phone3: public User3 {
public:
    void turn_on_phone3() override {
        cout << "User3 Device is on" << endl;
    }
    void take_photo()  {
        cout << "User 3 take_photo" << endl;
    }
    void call3() {
        cout << "User 3 is calling" << endl;
    }
};

int main() {
    Phone1 turn_on_phone1, call;
    turn_on_phone1.turn_on_phone1();
    call.call();

    Phone2 turn_on_phone2, browsing_internet;
    turn_on_phone2.turn_on_phone2();
    browsing_internet.browsing_internet();

    Phone3 turn_on_phone3, take_photo, call3;
    turn_on_phone3.turn_on_phone3();
    take_photo.take_photo();
    call3.call3();

    return 0;
}