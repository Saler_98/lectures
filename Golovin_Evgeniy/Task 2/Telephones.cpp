#include <iostream>

int main()
{
    class IPhone // abstract phone
    {
    public: virtual void get_price() = 0;
          virtual void get_name() = 0;
          void call() {
              std::cout << "Ring-ring\n";
          }
          virtual ~IPhone() {};
    };

    class IPhoto
    {
    public: virtual void photo() = 0;
          virtual ~IPhoto() {};
    };

    class IInternet
    {
    public: virtual void internet() = 0;
        virtual void pay() = 0;
        virtual ~IInternet() {};
    };

    class IUser // abstract user
    {
    public: virtual void use_phone() = 0;
          virtual ~IUser() {};
    };

    class Telephone : public IPhone // Phone inheritance
    {
    public:
        void get_price() override { // Sets Price
            std::cout << "1000\n";
        }
        void get_name() override { // Sets Name
            std::cout << "Telephone\n";
        }
    };

    class Photophone : public IPhone, IPhoto // Phone inheritance
    {
    public:
        void get_price() override { // Sets Price
            std::cout << "1500\n";
        }
        void get_name() override { // Sets Name
            std::cout << "Photophone\n";
        }
        void photo() override { // Function override
            std::cout << "Click\n";
        }
    };

    class Netphone : public IPhone, IInternet // Phone inheritance
    {
    public:
        void get_price() override { // Sets Price
            std::cout << "3000\n";
        }
        void get_name() override { // Sets Name
            std::cout << "Netphone\n";
        }
        void internet() override { // Function override
            std::cout << "Website\n";
        }
        void pay() override { // Function override
            std::cout << "Scam\n";
        }
    };

    class Normal : public IUser // User inheritance
    {
    public:
        Telephone phone;
        void use_phone() override { // Function override
            phone.call();
        }
    };

    class Photographer : public IUser // User inheritance
    {
    public:
        Photophone phone;
        void use_phone() override { // Function override
            phone.photo();
        }

    };

    class Victim : public IUser // User inheritance
    {
    public:
        Netphone phone;
        void use_phone() override { // Function override
            phone.internet();
            phone.pay();
        }

    };

    // A test

    Normal normal;
    Photographer photographer;
    Victim victim;

    std::cout << "Normal\n";
    normal.use_phone();
    std::cout << "\nPhotographer\n";
    photographer.use_phone();
    std::cout << "\nVictim\n";
    victim.use_phone();
};